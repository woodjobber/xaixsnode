//
//  XAxisNode.swift
//  XAxisNode
//
//  Created by chengbin on 2020/12/25.
//

import UIKit

///
/// 横向轴线排列节点
///
/// 支持`AutoLayout`布局, 可在`Xib`，`Storyboard`使用
///
public class XAxisNode: UIView {
    public struct Node {
        /// 节点值，位于`圆点`下部分
        public var value = ""
        
        /// 节点值描述文本，位于`节点值`下部分
        public var text = ""
        
        /// 是否是`当前`节点位置
        public var isCurrent: Bool = false
        
        /// 节点`rect`
        public var rect: CGRect {
            return _rect
        }

        /// 节点中心点
        public var center: CGPoint {
            return CGPoint(x: _rect.midX, y: _rect.midY)
        }

        /// 节点中心`x`
        public var centerX: CGFloat {
            return _rect.midX
        }

        /// 节点中心`y`
        public var centerY: CGFloat {
            return _rect.midY
        }
        
        private var _rect: CGRect = .zero
        
        /**
         Returns a newly initialized `XAxisNode.Node` instance .
         
         - Parameter value: 节点值
         - Parameter text: 节点值的描述文本
         - Parameter isCurrent: 是否为当前节点
         - Returns: An initialized `XAxisNode.Node` object or nil if the object couldn't be created.
         */
        public init(value: String, text: String, isCurrent: Bool) {
            self.value = value
            self.text = text
            self.isCurrent = isCurrent
        }

        /**
         Returns a newly initialized `XAxisNode.Node` instance .
         
         - Parameter value: 节点值
         - Parameter text: 节点值的描述文本
         - Returns: An initialized `XAxisNode.Node` object or nil if the object couldn't be created.
         */
        public init(value: String, text: String) {
            self.value = value
            self.text = text
            self.isCurrent = false
        }

        /**
         Returns a newly initialized `XAxisNode.Node` instance .
         
         - Parameter value: 节点值
         - Returns: An initialized `XAxisNode.Node` object or nil if the object couldn't be created.
         */
        public init(value: String) {
            self.value = value
            self.text = ""
            self.isCurrent = false
        }

        /// 默认初始化
        public init() {
            self.isCurrent = false
            self.text = ""
            self.value = ""
        }

        fileprivate mutating func _setRect(_ rect: CGRect) {
            _rect = rect
        }

        // 返回指向 node 实例头部的指针
        fileprivate mutating func headPointerOfStruct() -> UnsafeMutablePointer<Int8> {
            return withUnsafeMutablePointer(to: &self) {
                return UnsafeMutableRawPointer($0).bindMemory(to: Int8.self, capacity: MemoryLayout<Self>.stride)
            }
        }
    }
    
    /// 不可见的轴线，作用是做标线
    private let axisLine = UIView()
    
    /// 节点内容的上下左右边距，默认 `UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)`
    public var edgeInsets = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15) {
        didSet {
            setNeedsDisplay()
        }
    }
    
    /// 偏好最大宽度,如果使用`AutoLayout`布局，必须设置该属性`preferredMaxLayoutWidth>0`，配合`manualCalculateHeight=true`使用
    @IBInspectable public var preferredMaxLayoutWidth: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }

    /// 手动计算高度,需要配合 `preferredMaxLayoutWidth>0 `使用,`AutoLayout`有效
    @IBInspectable public var manualCalculateHeight: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }

    /// 节点中心变化回调
    private typealias StandByCallback = ((CGPoint) -> Void)
    
    /// 存储值节点
    fileprivate var values: [LabelNode] = []
    
    /// 存储文本节点或描述节点
    fileprivate var texts: [LabelNode] = []
    
    /// 存储圆点节点
    fileprivate var cycleNodes: [CycleNode] = []
    
    /// 存储节点之间的连接线
    fileprivate var nodeLines: [NodelLine] = []
    
    /// 当前节点
    public var currentNode: Node?
    
    /// 当前选中的节点，如果没有选中的，则为`-1`
    public var currentNodeIndex: Int = -1
    
    /// 节点中心变化回调
    private var onChangedNodeCenter: StandByCallback?
    
    /// 所有左边节点(圆点)颜色，以 `当前节点` 为界限
    public var leftNodeColor: UIColor = .white
    
    /// 所有右边节点(圆点)颜色，以 `当前节点` 为界限，默认`UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)`
    public var rightNodeColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)
    
    /// 当前节点(圆点)颜色，默认` UIColor.white`
    public var currentNodeColor: UIColor = .white
    
    /// 右节点连接线颜色，默认`UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)`
    public var rightNodeLineColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)
    
    /// 左节点连接颜色，默认`UIColor(red: 1, green: 1, blue: 1, alpha: 1)`
    public var leftNodeLineColor: UIColor = .white
    
    /// 节点值颜色，默认`UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)`
    public var nodeValueColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
    
    /// 节点描述文本颜色，默认`UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)`
    public var nodeTextColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
    
    /// 当前节点值颜色，默认`UIColor(red: 1, green: 1, blue: 1, alpha: 1)`
    public var currentNodeValueColor: UIColor = .white
    
    /// 当前节点描述文本颜色，默认`UIColor(red: 1, green: 1, blue: 1, alpha: 1)`
    public var currentNodeTextColor: UIColor = .white
    
    /// 节点值字体
    public var nodeValueFont: UIFont? = UIFont(name: "PingFang-SC-Regular", size: 12)
    
    /// 节点描述文本字体
    public var nodeTextFont: UIFont? = UIFont(name: "PingFang-SC-Regular", size: 12)
    
    /// 自适应内容大小 , 默认`false`
    public var isSelfSizing: Bool = false
    
    /// 节点数据源，如果为空，则使用默认值，数量为`5`个节点
    public var nodes: [Node] {
        set {
            _nodes = newValue
            setNeedsDisplay()
            setNeedsLayout()
            layoutIfNeeded()
        }
        get {
            return _nodes
        }
    }

    private var _nodes: [Node] = []
    
    public init() {
        super.init(frame: .zero)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
 
    /// 节点中心`x`变化 ，需要调用该函数，观察回调参数
    /// - Parameter standBy: 节点中心坐标变化回调
    public func onChangedCenter(standBy: @escaping ((_ center: CGPoint) -> Void)) {
        onChangedNodeCenter = standBy
    }

    /// 得到节点，返回不是`圆点视图`，是节点元素
    /// - Parameter indes: 节点`index`
    /// - Returns: 节点元素
    public func getNode(at index: Int) -> Node? {
        guard index < nodes.count, index >= 0 else {
            return nil
        }
        return nodes[index]
    }
    
    /// 节点所对应的`rect`
    /// - Parameter index: 节点元素 索引
    /// - Returns: 节点位于  `rect`
    public func rect(for index: Int) -> CGRect {
        let node = getNode(at: index)
        return rect(for: node)
    }

    /// 节点所对应的`rect`
    /// - Parameter node: 节点元素
    /// - Returns: 节点位于  `rect`
    public func rect(for node: Node?) -> CGRect {
        guard let aNode = node else {
            return .zero
        }
        let index = nodes.firstIndex { (n) -> Bool in
            n.isCurrent == aNode.isCurrent && n.text == aNode.text && n.value == aNode.value
        }
        if let idx = index {
            return cycleNodes[idx].frame
        }
        return .zero
    }

    /// 获取节点 的 中心`x`
    /// - Parameter node: 节点元素
    /// - Returns: 节点的 中心 `x`
    public func centerX(for node: Node?) -> CGFloat {
        guard let aNode = node else {
            return 0
        }
        let index = nodes.firstIndex { (n) -> Bool in
            n.isCurrent == aNode.isCurrent && n.text == aNode.text && n.value == aNode.value
        }
        if let idx = index {
            return cycleNodes[idx].frame.midX
        }
        return 0
    }

    /// 获取节点 的中心`y`
    /// - Parameter node: 节点元素
    /// - Returns: 节点的 中心 `y`
    public func centerY(for node: Node?) -> CGFloat {
        guard let aNode = node else {
            return 0
        }
        let index = nodes.firstIndex { (n) -> Bool in
            n.isCurrent == aNode.isCurrent && n.text == aNode.text && n.value == aNode.value
        }
        if let idx = index {
            return cycleNodes[idx].frame.midY
        }
        return 0
    }

    public func centerY(for index: Int) -> CGFloat {
        return centerY(for: getNode(at: index))
    }

    /// 获取节点的 中心 `center`
    /// - Parameter node: 节点元素
    /// - Returns: 节点的 中心
    public func center(for node: Node?) -> CGPoint {
        return CGPoint(x: centerX(for: node), y: centerY(for: node))
    }
    
    public func center(for index: Int) -> CGPoint {
        return CGPoint(x: centerX(for: getNode(at: index)), y: centerY(for: index))
    }

    /// 当前节点的`rect`
    /// - Returns: 当前节点 `rect`
    public func rectForCurrentNode() -> CGRect {
        guard currentNodeIndex < cycleNodes.count, currentNodeIndex >= 0 else {
            return .zero
        }
        return cycleNodes[currentNodeIndex].frame
    }

    /// 当前节点的中心`x`
    /// - Returns: 当前节点 `x`
    public func centerXForCurrentNode() -> CGFloat {
        return rectForCurrentNode().midX
    }
    
    /// 当前节点的中心`y`
    /// - Returns: 当前节点 `y`
    public func centerYForCurrentNode() -> CGFloat {
        return rectForCurrentNode().midY
    }

    /// 当前节点的中心`center`
    /// - Returns: 当前节点 `center`
    public func centerForCurrentNode() -> CGPoint {
        return CGPoint(x: centerXForCurrentNode(), y: centerYForCurrentNode())
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        _drawNode()
    }
    
    /// 绘制 节点
    public func draw() {
        setNeedsDisplay()
        setNeedsLayout()
        layoutIfNeeded()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    override public var intrinsicContentSize: CGSize {
        guard !nodes.isEmpty else {
            return .zero
        }
        var textNode = texts.filter { (node) -> Bool in
            if let text = node.text {
                return !text.isEmpty
            }
            return false
        }.first
        if textNode == nil {
            textNode = texts.first
        }

        var height: CGFloat = 0
        let constraint = constraints.filter { (c) -> Bool in
            c.isActive && c.firstAttribute == .height
        }.first
        
        if let c = constraint, c.firstAttribute == .height {
            height = c.constant
        }
        
        if let node = textNode {
            let height = node.frame.maxY + edgeInsets.bottom
            guard preferredMaxLayoutWidth > 0 else {
                return CGSize(width: frame.width, height: height)
            }
            return CGSize(width: preferredMaxLayoutWidth, height: height)
        }
        guard preferredMaxLayoutWidth > 0 else {
            return CGSize(width: frame.width, height: height)
        }
        return CGSize(width: preferredMaxLayoutWidth, height: height)
    }

    override public func prepareForInterfaceBuilder() {
        invalidateIntrinsicContentSize()
    }
    
    // MARK: - - Private
    
    private func commonInit() {
        addSubview(axisLine)
        axisLine.frame = CGRect(x: edgeInsets.left, y: edgeInsets.top, width: frame.width - edgeInsets.left - edgeInsets.right, height: 2)
        axisLine.backgroundColor = .clear
        axisLine.layer.cornerRadius = 1
        axisLine.layer.masksToBounds = true
        backgroundColor = .clear
    }
    
    /// 默认节点数据源` 5`个节点
    private func defaultNodes() -> [Node] {
        var _nodes: [Node] = []
        for _ in 0 ... 4 {
            let node = Node(value: "", text: "", isCurrent: false)
            _nodes.append(node)
        }
        return _nodes
    }

    /// 重置状态
    private func _reset() {
        cycleNodes.forEach { node in
            node.removeFromSuperview()
        }
        texts.forEach { node in
            node.removeFromSuperview()
        }
        values.forEach { node in
            node.removeFromSuperview()
        }
        nodeLines.forEach { node in
            node.removeFromSuperview()
        }
        cycleNodes.removeAll()
        texts.removeAll()
        values.removeAll()
        nodeLines.removeAll()
        
        currentNode = nil
        currentNodeIndex = -1
    }
    
    private func _drawNode() {
        axisLine.frame = CGRect(x: edgeInsets.left, y: edgeInsets.top, width: frame.width - edgeInsets.left - edgeInsets.right, height: 2)
        _reset()
        guard !frame.size.equalTo(.zero) else {
            return
        }
        
        var r = frame
        if preferredMaxLayoutWidth > 0 {
            r.size.width = preferredMaxLayoutWidth
        }
        
        if !r.equalTo(frame) {
            frame = r
        }
        
        /// 最左边节点线与最右边节点线宽度固定，圆点大小固定
        let cycleSize: CGFloat = 10
        let fixedNodeWidth: CGFloat = 51
        
        /// 创建所有节点线
        var nodes = self.nodes
        if nodes.isEmpty {
            nodes = defaultNodes()
        }
        for _ in 0 ... nodes.count {
            let nodeLine = NodelLine()
            addSubview(nodeLine)
            nodeLines.append(nodeLine)
        }
        let axisLineWidth = axisLine.frame.width
        let axisLineHeight = axisLine.frame.height
        /// 补偿宽度
        let scaleWidth: CGFloat = 0.5
        var _nodes = [Node]()
        for (index, var node) in nodes.enumerated() {
            let y = axisLine.frame.minY - (cycleSize - axisLineHeight) / 2
            
            let cycle = CycleNode(frame: CGRect(x: 0, y: 0, width: cycleSize, height: cycleSize))
            addSubview(cycle)
            cycleNodes.append(cycle)
            
            if index == 0 {
                cycle.frame = CGRect(x: edgeInsets.left + fixedNodeWidth, y: y, width: cycleSize, height: cycleSize)
            } else if index == nodes.count - 1 {
                /// 最后一个节点
                cycle.frame = CGRect(x: axisLineWidth + edgeInsets.left - fixedNodeWidth - cycleSize, y: y, width: cycleSize, height: cycleSize)
            } else {
                let width = axisLineWidth - (fixedNodeWidth + cycleSize - cycle.innerWidth * 2) * 2
                let nodeWidth: CGFloat = ceil(width / CGFloat(nodes.count - 1))
                let x = edgeInsets.left + fixedNodeWidth + nodeWidth * CGFloat(index)
                cycle.frame = CGRect(x: x, y: y, width: cycleSize, height: cycleSize)
            }
            
            node._setRect(cycle.frame)
            
            if node.isCurrent {
                cycle.fillRule = .evenOdd
                currentNodeIndex = index
                currentNode = node
            } else {
                cycle.fillRule = .filled
            }
            _nodes.append(node)
            let nodeLine = nodeLines[index]
            var lastNodeLine: NodelLine?
            if index == nodes.count - 1 {
                /// 最后一个节点
                let nodeLine = nodeLines[index + 1]
                let cycle = cycleNodes[index]
                let innerWidth = cycle.innerWidth
                let width = axisLineWidth + edgeInsets.left - (cycle.frame.maxX - innerWidth) + scaleWidth
                nodeLine.frame = CGRect(x: cycle.frame.maxX - innerWidth, y: axisLine.frame.origin.y, width: width, height: axisLineHeight)
                lastNodeLine = nodeLine
            }
            /// 根据当前节点，渲染节点（圆点）、节点线的颜色
            if currentNodeIndex >= 0, index > currentNodeIndex {
                cycle.fillColor = rightNodeColor
                nodeLine.backgroundColor = rightNodeLineColor
                lastNodeLine?.backgroundColor = rightNodeLineColor
            } else {
                cycle.fillColor = node.isCurrent ? currentNodeColor : leftNodeColor
                nodeLine.backgroundColor = leftNodeLineColor
                lastNodeLine?.backgroundColor = node.isCurrent ? rightNodeLineColor : leftNodeLineColor
            }
            
            let innerWidth = cycle.innerWidth
            /// 节点连接线
            if index == 0 {
                nodeLine.frame = CGRect(x: axisLine.frame.origin.x, y: axisLine.frame.origin.y, width: cycle.frame.minX - axisLine.frame.origin.x + innerWidth + scaleWidth, height: axisLineHeight)
            } else {
                let preCycleNode = cycleNodes[index - 1]
                nodeLine.frame = CGRect(x: preCycleNode.frame.maxX - innerWidth - scaleWidth, y: axisLine.frame.origin.y, width: cycle.frame.minX - preCycleNode.frame.maxX + innerWidth * 2 + scaleWidth + scaleWidth / 2, height: axisLineHeight)
            }
            
            /// 值节点
            let valueNode = LabelNode()
            valueNode.text = node.value
            valueNode.font = nodeValueFont
            valueNode.sizeToFit()
            var valueNodeframe = valueNode.frame
            valueNodeframe.origin.y = cycle.frame.maxY + 6
            valueNode.frame = valueNodeframe
            valueNode.center.x = cycle.center.x
            valueNode.nodeColor = nodeValueColor
            valueNode.currentNodeColor = currentNodeValueColor
            valueNode.isCurrent = node.isCurrent
            values.append(valueNode)
            addSubview(valueNode)
            
            /// 文本节点
            let textNode = LabelNode()
            textNode.text = node.text
            textNode.font = nodeTextFont
            textNode.nodeColor = nodeTextColor
            textNode.currentNodeColor = currentNodeTextColor
            textNode.isCurrent = node.isCurrent
            textNode.sizeToFit()
            var textNodeFrame = textNode.frame
            textNodeFrame.origin.y = valueNode.frame.maxY + 2
            textNode.frame = textNodeFrame
            textNode.center.x = cycle.center.x
            texts.append(textNode)
            addSubview(textNode)
        }
        /// 没有标注，则改变背景颜色
        if currentNodeIndex < 0 {
            for node in nodeLines {
                node.backgroundColor = rightNodeLineColor
            }
            for node in cycleNodes {
                node.fillColor = rightNodeColor
            }
        }
        if preferredMaxLayoutWidth > 0, manualCalculateHeight {
            invalidateIntrinsicContentSize()
        }
        var textNode: LabelNode?
        if currentNodeIndex >= 0 {
            textNode = texts[currentNodeIndex]
        } else {
            textNode = texts.filter { (node) -> Bool in
                if let text = node.text {
                    return !text.isEmpty
                }
                return false
            }.first
            if textNode == nil {
                textNode = texts.first
            }
        }

        if let node = textNode {
            let height = node.frame.maxY + edgeInsets.bottom
            var f = frame
            f.size.height = height
            if !frame.equalTo(frame), isSelfSizing, !existConatriantHeight() {
                frame = f
            } else if frame.height < height,!frame.equalTo(f), !existConatriantHeight() {
                frame = f
            }
        }
        let count = nodes.count
        
        if !self._nodes.isEmpty, count > 0, self._nodes.count == count {
            for index in 0 ..< count {
                self._nodes[index] = _nodes[index]
            }
        }
        let center = CGPoint(x: centerXForCurrentNode(), y: centerYForCurrentNode())
        onChangedNodeCenter?(center)
    }

    /// 判断是否存在 高度约束
    private func existConatriantHeight() -> Bool {
        let constraints = self.constraints
        guard !constraints.isEmpty else {
            return false
        }
        var exist = false
        for constraint in constraints {
            if constraint.firstAttribute == .height, constraint.secondAttribute == .notAnAttribute {
                exist = true
                break
            }
        }
        return exist
    }
}

public extension XAxisNode {
    /// 转换节点坐标系 到 指定 视图 坐标系
    ///
    /// - Parameter rect: 节点的`rect`，一个节点的`rect` 是相对于`XAxisNode`的坐标系
    /// - Parameter view: 目标视图坐标系
    /// - Returns: 返回一个相对于目标视图坐标系的坐标
    func convertNode(_ rect: CGRect, to view: UIView) -> CGRect {
        return convert(rect, to: view)
    }
}

/// 节点连接线
private class NodelLine: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        backgroundColor = .white
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        layer.masksToBounds = true
    }
}

/// 标签节点
private class LabelNode: UILabel {
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }

    var currentNodeColor: UIColor = .white
    
    var nodeColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
    
    var isCurrent: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    func commonInit() {
        font = UIFont(name: "PingFang-SC-Regular", size: 12)
        textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        backgroundColor = .clear
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if isCurrent {
            textColor = currentNodeColor
        } else {
            textColor = nodeColor
        }
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
}

/// 圆点节点
private class CycleNode: UIView {
    enum FillRule {
        /// 空心
        case evenOdd
        /// 实心
        case filled
    }
    
    let innerWidth: CGFloat = 2
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }
    
    init(frame: CGRect, fillRule: FillRule) {
        super.init(frame: frame)
        self.fillRule = fillRule
        backgroundColor = .clear
    }
    
    var fillRule: FillRule = .filled {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var fillColor: UIColor = .white {
        didSet {
            setNeedsDisplay()
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        backgroundColor = .clear
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let frame = rect.insetBy(dx: innerWidth, dy: innerWidth)
        let ctx = UIGraphicsGetCurrentContext()
        switch fillRule {
        case .evenOdd:
            ctx?.setLineWidth(innerWidth)
            ctx?.setStrokeColor(fillColor.cgColor)
            ctx?.strokeEllipse(in: frame)
        case .filled:
            ctx?.addEllipse(in: frame)
            ctx?.setFillColor(fillColor.cgColor)
            ctx?.fillPath()
        }
    }
}
