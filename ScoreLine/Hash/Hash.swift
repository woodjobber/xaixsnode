//
//  Hash.swift
//  ScoreLine
//
//  Created by chengbin on 2021/2/8.
//

import Foundation
///  如果两个不同的对象具有相同的哈希值时，会产生哈希冲突。当发生哈希冲突时，它们将存储在该地址对应的列表中。对象之间发生冲突的概率越高，哈希集合的性能就会更加线性增长
struct Color {
    let red: UInt8
    let green: UInt8
    let blue: UInt8
}

extension Color: Equatable {
    static func == (lhs: Color, rhs: Color) -> Bool {
        return lhs.red == rhs.red && lhs.green == rhs.green && lhs.blue == rhs.blue
    }
}
extension Color: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.red)
        hasher.combine(self.blue)
        hasher.combine(self.red)
    }
}


class ClassA {
    var name: String = ""

}
extension ClassA: Hashable,Equatable {
    static func == (lhs: ClassA, rhs: ClassA) -> Bool {
        return lhs.name == rhs.name
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}


/// NSObject 默认遵循了 Equatable, Hashable，
class ClassB: NSObject {
    var name: String = ""
    override func isEqual(_ object: Any?) -> Bool {
        guard let o  = object as? ClassB else {
            return false
        }
        if o === self {
            return true
        }
        
        return o.name == self.name
    }
    override var hash: Int {
        return name.hashValue
    }
}

class ClassC: NSObject {
    var name: String = ""
    static func == (lhs: ClassC, rhs: ClassC) -> Bool {
        return lhs.name == rhs.name
    }
}
