//
//  ViewController.swift
//  ScoreLine
//
//  Created by chengbin on 2020/12/25.
//

import UIKit
import XYAxisSDK
import GDPerformanceView_Swift
import Alamofire
import Moya


enum TestApi {
    case config
}
extension TestApi: TargetType {
    var sampleData: Data {
        return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
    }
    
    var baseURL: URL {
        return URL(string: "http://yun.banmago.com/api/mobile")!
    }
    
    var path: String {
        return "/common/config"
    }
    
    var method: Moya.Method {
        .get
    }

    
    var task: Task {
        return .requestParameters(parameters: ["email": "474436731@qq.com"], encoding: URLEncoding.default)
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    
}
/// https://medium.com/swiftindia/build-a-custom-universal-framework-on-ios-swift-549c084de7c8
/// https://appracatappra.com/2018/03/building-cross-platform-universal-frameworks-using-swift/
class ViewController: UIViewController {
    let axisNode = XAxisNode()
    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var detailLabel: UILabel!
    
    @IBOutlet weak var bottomConainer: UIView!
    
    var deliveringNotifications: DeliveringNotifications = DeliveringNotifications()
    
    var tableView: UITableView = UITableView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), style: .plain)
    
    var items: [YAxisTiledActionView.TiledItem] = [YAxisTiledActionView.TiledItem]()
    
    
    @objc func goToViper() {
        self.navigationController?.pushViewController(UsersRouter.createModule(), animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let monitor = PerformanceMonitor.shared()
        monitor.performanceViewConfigurator.options = .performance
        monitor.start()
        
        view.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        var nodes: [XAxisNode.Node] = []
  
        let node = XAxisNode.Node(value: "600")
        nodes.append(node)
        
        let node2 = XAxisNode.Node(value: "800")
        nodes.append(node2)

        let node3 = XAxisNode.Node(value: "823", text: "当前", isCurrent: true)
        nodes.append(node3)
 
        let node4 = XAxisNode.Node(value: "855", text: "上期")
        nodes.append(node4)

        let node5 = XAxisNode.Node(value: "900", text: "")
        nodes.append(node5)
       
        axisNode.nodes = nodes
        axisNode.backgroundColor = .clear
        container.addSubview(axisNode)
        
        axisNode.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            axisNode.topAnchor.constraint(equalTo: container.topAnchor, constant: 162),
            axisNode.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 0),
            axisNode.heightAnchor.constraint(equalToConstant: 80),
            axisNode.rightAnchor.constraint(equalTo: container.rightAnchor, constant: 0)
        ]

        NSLayoutConstraint.activate(constraints)
       
        let subView = UIView()
        subView.backgroundColor = .clear
        /// container.frame.maxY - 141 重合
        subView.frame = CGRect(x: 0, y: container.frame.maxY - 20, width: 8, height: 8)
        axisNode.onChangedCenter { [weak self] _ in
            guard let self = self else { return }
//            subView.center.x = center.x
            let rect = self.axisNode.rectForCurrentNode()
            let r = self.axisNode.convertNode(rect, to: self.view)
            subView.center.x = r.midX
            self.shakeAnimation(for: subView)
        }
      
        subView.layer.cornerRadius = 4
        subView.layer.borderWidth = 2
        subView.layer.borderColor = UIColor.red.cgColor
        view.addSubview(subView)
    
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let constraints3 = [
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0)
        ]
        tableView.isHidden = true
        NSLayoutConstraint.activate(constraints3)
        
        /// 动作列表
        let item1 = YAxisTiledActionView.TiledItem(text: "违法举报：对违法违规车辆进行拍照举报，主动参与共同维护良好交通环境。", actionTitle: "违法举报")
        let item2 = YAxisTiledActionView.TiledItem(text: "设施报修：对发生故障的交通公共设施进行报修，主动参与保障交通安全与便利。", actionTitle: "设施报修")
       
        items.append(item1)
        items.append(item2)
        
        tableView.register(XYAxisNodeCell.nib, forCellReuseIdentifier: "XYAxisNodeCell")
        tableView.estimatedRowHeight = 69.2
    
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        Memory.printAll()
//        let url = URL(string: "http://yun.banmago.com/api/mobile/common/config")!
//        AF.request(url,method: .get,parameters: ["email":""]).validate().responseJSON { (response) in
//            print(response.request?.url)
//        }
        
//        let provider = MoyaProvider<TestApi>.init( plugins:commonPlugins())
//        provider.request(.config) { (result) in
//            switch result {
//            case let .success(moyaResponse):
//                print(moyaResponse.response?.url?.absoluteString as Any)
//            case .failure:
//                break
//            }
//        }
//
        let tap = UITapGestureRecognizer(target: self, action: #selector(goToViper))
        container.addGestureRecognizer(tap)
    }
    public func commonPlugins() -> [PluginType] {
        let loggerPlugin = NetworkLoggerPlugin(configuration: .init(formatter: .init(responseData: JSONResponseDataFormatter), logOptions: .verbose))

        return [loggerPlugin]
    }
    private func JSONResponseDataFormatter(_ data: Data) -> String {
        do {
            let dataAsJSON = try JSONSerialization.jsonObject(with: data)
            let prettyData = try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
            return String(data: prettyData, encoding: .utf8) ?? String(data: data, encoding: .utf8) ?? ""
        } catch {
            return String(data: data, encoding: .utf8) ?? ""
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    // 左右摇摆
    func shakeAnimation(for view: UIView) {
        let layer = view.layer
        let position = layer.position
        let x = CGPoint(x: position.x + 5 , y: position.y)
        let y = CGPoint(x: position.x - 5, y: position.y)
        let animation = CABasicAnimation(keyPath: "position")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.fromValue = NSValue(cgPoint: x)
        animation.toValue = NSValue(cgPoint: y)
        animation.autoreverses = true
        animation.duration = 0.1
        animation.repeatCount = Float.greatestFiniteMagnitude
        layer.add(animation, forKey: nil)
    }
}
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "XYAxisNodeCell", for: indexPath) as! XYAxisNodeCell
        cell.actionView.items = items
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 15
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

