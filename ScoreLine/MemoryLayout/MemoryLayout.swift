//
//  MemoryLayout.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/14.
//

import UIKit

enum Kind {
    case wolf
    case fox
    case dog
    case sheep
}

struct Animal {
    private var a: Int = 1 // 8
    var b: String = "a" // 16
    var c: Kind = .wolf // 1
    var d: String? = ""// 16
    var e: Int8 = 8 // 1
    // size: 8+16+8+16+1 = 49
    //返回指向 Animal 实例头部的指针
    mutating func headPointerOfStruct() -> UnsafeMutablePointer<Int8> {
       return withUnsafeMutablePointer(to: &self) {
           return UnsafeMutableRawPointer($0).bindMemory(to: Int8.self, capacity: MemoryLayout<Self>.stride)
       }
    }
}
class Human {
    
    var age: Int?
    var name: String?
    var nicknames: [String] = [String]()

    //返回指向 Human 实例头部的指针
    func headPointerOfClass() -> UnsafeMutablePointer<Int8> {
        let opaquePointer = Unmanaged.passUnretained(self as AnyObject).toOpaque()
        let mutableTypedPointer = opaquePointer.bindMemory(to: Int8.self, capacity: MemoryLayout<Human>.stride)
        return UnsafeMutablePointer<Int8>(mutableTypedPointer)
    }
}

struct Puppy {
    let age: Int
    let isTrained: Bool
    let size: Double
    
}

struct Memory {
    var b: Bool = false
    var a: Int = 0
    static func printAll() {
        print(MemoryLayout<Animal>.size)
        print(MemoryLayout<Animal>.stride)
        print(MemoryLayout<Animal>.alignment)
        
        print(MemoryLayout<Wolf>.size)
        print(MemoryLayout<Wolf>.stride)
        print(MemoryLayout<Wolf>.alignment)
        var animal = Animal()
        
        let animalPtr: UnsafeMutablePointer<Int8> = animal.headPointerOfStruct()
        let animalRawPtr = UnsafeMutableRawPointer(animalPtr)
        let intValueFormJosn = "vd"
        let aPtr = animalRawPtr.advanced(by: 32).assumingMemoryBound(to: String.self)
        aPtr.initialize(to: intValueFormJosn)
    
        print(animal.d ?? "")
        
        
        let human = Human()
        let arrFormJson = ["goudan","zhaosi", "wangwu"]

        //拿到指向 human 堆内存的 void * 指针
        let humanRawPtr = UnsafeMutableRawPointer(human.headPointerOfClass())

        //nicknames 数组在内存中偏移 48byte 的位置(16 + 16 + 16)
        let humanNickNamesPtr = humanRawPtr.advanced(by: 48).assumingMemoryBound(to: Array<String>.self)
        humanNickNamesPtr.initialize(to: arrFormJson)
        print(human.nicknames)
        
        
        
        let wolf = Wolf()
        let wolfPtr = UnsafeMutableRawPointer(wolf.headPointerOfClass())

        let fox = Fox()
        let foxPtr = UnsafeMutableRawPointer(fox.headPointerOfClass())
        foxPtr.advanced(by: 0).bindMemory(to: UnsafeMutablePointer<Wolf.Type>.self, capacity: 1).initialize(to: wolfPtr.advanced(by: 0).assumingMemoryBound(to: UnsafeMutablePointer<Wolf.Type>.self).pointee)

        print(type(of: fox))
        
        fox.soul()
        print(class_getInstanceSize(Person.self))
                
    }
}
class Wolf {
    var name: String = "wolf"

    func soul() {
        print("my soul is wolf")
    }

    func headPointerOfClass() -> UnsafeMutablePointer<Int8> {
        let opaquePointer = Unmanaged.passUnretained(self as AnyObject).toOpaque()
        let mutableTypedPointer = opaquePointer.bindMemory(to: Int8.self, capacity: MemoryLayout<Wolf>.stride)
        return UnsafeMutablePointer<Int8>(mutableTypedPointer)
    }
}
        
class Fox {
    var name: String = "fox"
    
    func soul() {
        print("my soul is fox")
    }
    func headPointerOfClass() -> UnsafeMutablePointer<Int8> {
        let opaquePointer = Unmanaged.passUnretained(self as AnyObject).toOpaque()
        let mutableTypedPointer = opaquePointer.bindMemory(to: Int8.self, capacity: MemoryLayout<Fox>.stride)
        return UnsafeMutablePointer<Int8>(mutableTypedPointer)
    }
}

class Person: NSObject {
    var nice: Bool = false
    
//    var name: String = "fox" // 16
//    var age: Int = 0 // 4 内存对齐 8
    ///  print(class_getInstanceSize(Person.self))  output: 40  = 16  + 16  + 8
}
