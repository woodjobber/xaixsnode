//
//  XYAxisNodeCell.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/11.
//

import UIKit
import XYAxisSDK

class XYAxisNodeCell: UITableViewCell {
    var actionView = YAxisTiledActionView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        actionView.clikedSenderCompletion = {
            index in
            print(index)
        }
        contentView.addSubview(actionView)
        actionView.translatesAutoresizingMaskIntoConstraints = false
        actionView.manualCalculateHeight = true
        actionView.preferredMaxLayoutWidth = UIScreen.main.bounds.width
        let constraints2 = [
            actionView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            actionView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 0),
            actionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            actionView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 0)
        ]
        NSLayoutConstraint.activate(constraints2)
    }

}


public protocol NibLoadable: NSObjectProtocol & AnyObject {
    static var nibName:String {get}
    static var nib: UINib {get}
}
extension NibLoadable {
    public static func instance() -> Self {
         let nibObjects = nib.instantiate(withOwner: nil, options: nil)
         let nibs = nibObjects.filter { (nib) -> Bool in
             if let _ = nib as? Self {
                 return true
             }
             return false
         }
         return nibs.first as! Self
     }
    public static func nib(bundle: Bundle? = Bundle.main) -> UINib {
         return UINib(nibName: nibName, bundle: bundle)
     }
    public static var nibName: String {
         return String(describing: self)
     }
    public static var nib: UINib {
         return nib()
     }
}
extension UITableViewCell: NibLoadable {}
