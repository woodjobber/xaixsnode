//
//  DeliveringNotifications.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/7.
//

import UIKit
/// 参考：
/// Link: https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/Notifications/Articles/Threading.html#//apple_ref/doc/uid/20001289-CEGJFDFG
/// 接收到的通知放到指定线程中
class DeliveringNotifications: NSObject, NSMachPortDelegate {
    var notifications: [NSNotification] = [NSNotification]()
    var notificationThread: Thread = Thread.main // 目标主线程
    var notificationLock: NSLock = NSLock()
    var notificationPort: NSMachPort = NSMachPort()
    func setupThreadingSupport() {
        notificationPort.setDelegate(self)
        /// 端口 添加到 主线程 RunLoop中
        RunLoop.main.add(self.notificationPort, forMode: .common)
        NotificationCenter.default.addObserver(self, selector: #selector(processNotification(_:)), name: NSNotification.Name("name"), object: nil)
    }
    /// 收到端口通信的消息
    func handleMachMessage(_ msg: UnsafeMutableRawPointer) {
        notificationLock.lock()
        while notifications.count > 0 {
            let notification = notifications.first!
            notifications.removeFirst()
            notificationLock.unlock()
            processNotification(notification)
            notificationLock.lock()
        }
    }
    /// 处理通知
    @objc func processNotification(_ notification: NSNotification) {
        if Thread.current != notificationThread {
            print(Thread.current)
            notificationLock.lock()
            notifications.append(notification)
            notificationLock.unlock()
            notificationPort.send(before: Date(), components: nil, from: nil, reserved: 0)
        }else {
            print(Thread.current)
        }
    }
}
