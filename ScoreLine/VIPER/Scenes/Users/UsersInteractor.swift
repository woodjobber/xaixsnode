//
//  UsersInteractor.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/2.
//

import Foundation
/// @see https://infinum.com/handbook/books/ios/viper/viper-and-best-practices
class UsersInteractor: UsersInteractorInputProtocol {
    weak var presenter: UsersInteractorOutputProtocol?
    
    private let usersWorker = UsersWorker()
    
    func getUsers() {
        usersWorker.getUsers { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let users):
                self.presenter?.usersFetchedSuccessfully(users: users)
            case .failure(let error):
                self.presenter?.usersFetchingFailed(withError: error)
            }
        }
    }
    
}
