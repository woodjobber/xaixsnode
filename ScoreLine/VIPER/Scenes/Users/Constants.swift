//
//  Constants.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/3.
//

import Foundation

struct Constants {
    
    static let baseURL = "https://api.mocki.io/v1/914cd82b"
    
    static let usersEndpoint = "/viper/user"
}
