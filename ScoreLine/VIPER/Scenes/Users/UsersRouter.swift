//
//  UsersRouter.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/2.
//

import UIKit

class UsersRouter: UsersRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(UsersViewController.self)") as! UsersViewController
        let interactor = UsersInteractor()
        let router = UsersRouter()
        let presenter = UsersPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func openModule() {
        viewController?.navigationController?.pushViewController(UsersRouter.createModule(), animated: true)
    }
    
}
