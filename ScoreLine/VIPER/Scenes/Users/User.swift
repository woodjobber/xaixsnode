//
//  User.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/3.
//

import Foundation

class User: Codable {
    var id: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var avatar: String = ""
    var createdAt: Int = 0
}

struct UserViewModel {
    
    var fullName: String
    var avatar: URL?
    var registrationDate: String
    
    let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-DD"
        return dateFormatter
    }()
    
    init(user: User) {
        fullName = (user.firstName ) + (user.lastName)
        avatar = URL(string: user.avatar)
        registrationDate = "\(dateFormatter.string(from: Date(timeIntervalSince1970: Double(user.createdAt))))"
    }
    
}
