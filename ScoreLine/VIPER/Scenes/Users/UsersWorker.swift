//
//  UsersWorker.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/3.
//

import Foundation

class UsersWorker {
    
    private let networkLayer = NetworkLayer()
    
    func getUsers(completionHandler: @escaping (Result<[User]>) -> ()) {
        networkLayer.request(UserRouter.get, decodeToType: [User].self, completionHandler: completionHandler)
    }
    
}
