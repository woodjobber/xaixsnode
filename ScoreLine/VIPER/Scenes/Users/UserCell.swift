//
//  UserCell.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/2.
//

import UIKit
import Kingfisher

class UserCell: UITableViewCell, UsersViewInputProtocol {
    
    func configure(viewModel: UserViewModel) {
        let url = viewModel.avatar!
        arvatarImageView.kf.setImage(with: url)
        dateLabel.text = viewModel.registrationDate
        nameLabel.text = viewModel.fullName
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var arvatarImageView: UIImageView!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
