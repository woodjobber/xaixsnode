//
//  UsersProtocol.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/2.
//

import Foundation

/// UIViewController
protocol UsersViewProtocol: UsersViewInputProtocol {
    /// 发送事件给 presenter
    var presenter: UsersPresenterProtocol! {get set}
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func reloadData()
}
/// Presenter
protocol UsersPresenterProtocol: UsersViewOutputProtocol {
    /// 通知View更新
    var view: UsersViewProtocol? {get set}
    func viewDidLoad()
    func configure(cell: UsersViewInputProtocol, indexPath: IndexPath)
    func numberOfRows() -> Int
}
/// Router
protocol UsersRouterProtocol: class {
    func openModule()
}
/// Interactor
protocol UsersInteractorInputProtocol: class {
    /// 发送事件给presenter
    var presenter: UsersInteractorOutputProtocol? {get set}
    func getUsers()
}
protocol UsersInteractorOutputProtocol: class {
    func usersFetchedSuccessfully(users: [User])
    func usersFetchingFailed(withError error: Error)
}
/// View input
protocol UsersViewInputProtocol: class {
    func configure(viewModel:UserViewModel)
}
/// View output
protocol UsersViewOutputProtocol: class  {
    func didSelectRowAt (indexPath: IndexPath)
}
