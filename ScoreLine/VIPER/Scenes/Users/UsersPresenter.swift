//
//  UsersPresenter.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/2.
//

import Foundation

class UsersPresenter: UsersPresenterProtocol, UsersInteractorOutputProtocol, UsersViewOutputProtocol {
    // MARK: - UsersViewOutputProtocol
    func didSelectRowAt(indexPath: IndexPath) {
        ///到另一个模块
        router.openModule()
    }

    
    //MARK: - UsersInteractorOutputProtocol
    func usersFetchedSuccessfully(users: [User]) {
        view?.hideLoadingIndicator()
        self.users.append(contentsOf: users)
        view?.reloadData()
    }
    
    func usersFetchingFailed(withError error: Error) {
        view?.hideLoadingIndicator()
    }
    
    weak var view: UsersViewProtocol?
    private let interactor: UsersInteractorInputProtocol
    private let router: UsersRouterProtocol
    private var users = [User]()
    init(view: UsersViewProtocol, interactor: UsersInteractorInputProtocol, router: UsersRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    func viewDidLoad() {
        view?.showLoadingIndicator()
        interactor.getUsers()
    }
    func configure(cell: UsersViewInputProtocol, indexPath: IndexPath) {
        let user = users[indexPath.row]
        cell.configure(viewModel: UserViewModel(user: user))
    }
    func numberOfRows() -> Int {
        return users.count
    }
    
}
