//
//  PropertyWrapper.swift
//  ScoreLine
//
//  Created by chengbin on 2021/1/5.
//

import Foundation
/// MARK -  动态成员 @dynamicMemberLookup

class Texture {
    var isSparkly: Bool = false
    var text: String = ""
    
}

extension Texture: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let texture = Texture()
        texture.isSparkly = isSparkly
        return texture
    }
}

@dynamicMemberLookup
struct Material {
    private var _texture: Texture
    init(texture: Texture) {
        self._texture = texture
    }
    subscript(dynamicMember member: String) -> String {
        let properties = ["name": "Taylor Swift", "city": "Nashville"]
        return properties[member, default: ""]
    }

    subscript(dynamicMember member: String) -> Int {
        let properties = ["age": 26, "height": 178]
        return properties[member, default: 0]
    }

    subscript(dynamicMember member: String) -> (_ input: String) -> Void {
        return {
            print("Hello! I live at the address \($0).")
        }
    }

    subscript<T>(dynamicMember keyPath: ReferenceWritableKeyPath<Texture, T>) -> T {
        get {
            _texture[keyPath: keyPath]
        }
        set {
            if !isKnownUniquelyReferenced(&_texture) {
                _texture = _texture.copy() as! Texture
            }
            _texture[keyPath: keyPath] = newValue
        }
    }
}
/// https://www.avanderlee.com/swift/dynamic-member-lookup/
@dynamicMemberLookup
struct NotificationWrapper {
    let notification: Notification

    subscript(dynamicMember string: String) -> String {
        return notification.userInfo?[string] as? String ?? ""
    }
    subscript(dynamicMember string: String) -> Int {
        return notification.userInfo?[string] as? Int ?? 0
    }
}
struct Blog {
    let title: String
    let url: URL
}

@dynamicMemberLookup
struct Blogger {
    let name: String
    let blog: Blog

    subscript<T>(dynamicMember keyPath: KeyPath<Blog, T>) -> T {
        return blog[keyPath: keyPath]
    }
}

/// https://nshipster.com/propertywrapper/
@propertyWrapper
struct UserDefault<Value> {
    let key: String
    let defaultValue: Value
    var container: UserDefaults = .standard

    var wrappedValue: Value {
        get {
            return container.object(forKey: key) as? Value ?? defaultValue
        }
        set {
            container.set(newValue, forKey: key)
        }
    }
}
extension UserDefaults {

    @UserDefault(key: "has_seen_app_introduction", defaultValue: false)
    static var hasSeenAppIntroduction: Bool
}

@propertyWrapper
struct SampleFile {
    let fileName: String
    var wrappedValue: URL? {
        let file = fileName.split(separator: ".").first!
        let fileExtension = fileName.split(separator: ".").last!
        let url = Bundle.main.url(forResource: String(file), withExtension: String(fileExtension))
        return url
    }
    /// 映射值
    var projectedValue: String {
        return fileName
    }
}

struct SampleFiles {
    @SampleFile(fileName: "img_loading.png")
    static var image: URL?
}

@propertyWrapper
struct LateInitialized<Value> {
    private(set) var storage: Value
    var wrappedValue: Value {
        get {
            return storage
        }
        set {
            storage = newValue
        }
    }
}

@propertyWrapper
struct Trimmed {
    private(set) var value: String = ""

    var wrappedValue: String {
        get { value }
        set { value = newValue.trimmingCharacters(in: .whitespacesAndNewlines) }
    }

    init(wrappedValue initialValue: String) {
        self.wrappedValue = initialValue
    }
}

struct Post {
    @Trimmed var title: String
    @Trimmed var body: String
}

