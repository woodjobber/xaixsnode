//
//  XYAxisSDK+Button.swift
//  XYAxisSDK
//
//  Created by chengbin on 2020/12/30.
//

import UIKit
///
///
/// 图片位置
public enum ImageEdge {
    case left
    case right
    case top
    case bottom
    /// 图片与按钮垂直居中，注意： 如果按钮高度过小，会自动自适应内容高度
    case center
}

public extension UIButton {
    /// 调整按钮的图片与标题的位置关系
    /// - Parameter edge: 图片位置
    /// - Parameter space: 图片与标题间隔
    func imageEdge(_ edge: ImageEdge, space: CGFloat = 5) {
        let imageViewWidth = imageView?.frame.width ?? 0
        var titleLabelWidth = titleLabel?.frame.width ?? 0
        if titleLabelWidth == 0, let titleLabel = titleLabel {
            let text = (titleLabel.text ?? "") as NSString
            titleLabelWidth = text.size(withAttributes: [NSAttributedString.Key.font: titleLabel.font as Any]).width
        }
        var imageInsetsTop: CGFloat = 0
        var imageInsetsLeft: CGFloat = 0
        var imageInsetsBottom: CGFloat = 0
        var imageInsetsRight: CGFloat = 0
        
        var titleInsetsTop: CGFloat = 0
        var titleInsetsLeft: CGFloat = 0
        var titleInsetsRight: CGFloat = 0
        var titleInsetsBottom: CGFloat = 0
        
        var space = space
        
        switch edge {
        case .right:
            space = space * 0.5
            imageInsetsLeft = titleLabelWidth + space
            imageInsetsRight = -imageInsetsLeft
            
            titleInsetsLeft = -(imageViewWidth + space)
            titleInsetsRight = -titleInsetsLeft
        case .left:
            space = space * 0.5
            imageInsetsLeft = -space
            imageInsetsRight = -imageInsetsLeft
            
            titleInsetsLeft = space
            titleInsetsRight = -titleInsetsLeft
        case .bottom:
            let imageViewHeight = imageView?.frame.height ?? 0
            let titleLabelHeight = titleLabel?.frame.height ?? 0
            let buttonHeight = frame.height
            let boundsCenter = (imageViewHeight + space + titleLabelHeight) * 0.5
            
            let centerX_button = bounds.midX
            let centerX_titleLabel = titleLabel?.frame.midX ?? 0
            let centerX_image = imageView?.frame.midX ?? 0
            
            let imageBottomY = imageView?.frame.maxY ?? 0
            let titleTopY = titleLabel?.frame.minY ?? 0
            
            imageInsetsTop = buttonHeight - (buttonHeight * 0.5 - boundsCenter) - imageBottomY
            imageInsetsLeft = centerX_button - centerX_image
            imageInsetsRight = -imageInsetsLeft
            imageInsetsBottom = -imageInsetsTop
            
            titleInsetsTop = (buttonHeight * 0.5 - boundsCenter) - titleTopY
            titleInsetsLeft = -(centerX_titleLabel - centerX_button)
            titleInsetsRight = -titleInsetsLeft
            titleInsetsBottom = -titleInsetsTop
            
        case .top:
            let imageViewHeight = imageView?.frame.height ?? 0
            let titleLabelHeight = titleLabel?.frame.height ?? 0
            let buttonHeight = frame.height
            let boundsCenter = (imageViewHeight + space + titleLabelHeight) * 0.5
            
            let centerX_button = bounds.midX
            let centerX_titleLabel = titleLabel?.frame.midX ?? 0
            let centerX_image = imageView?.frame.midX ?? 0
            
            let imageTopY = imageView?.frame.minY ?? 0
            let titleBottom = titleLabel?.frame.maxY ?? 0
            
            imageInsetsTop = (buttonHeight * 0.5 - boundsCenter) - imageTopY
            imageInsetsLeft = centerX_button - centerX_image
            imageInsetsRight = -imageInsetsLeft
            imageInsetsBottom = -imageInsetsTop
            
            titleInsetsTop = buttonHeight - (buttonHeight * 0.5 - boundsCenter) - titleBottom
            titleInsetsLeft = -(centerX_titleLabel - centerX_button)
            titleInsetsRight = -titleInsetsLeft
            titleInsetsBottom = -titleInsetsTop
        case .center:
            let imageSize = imageView?.frame.size ?? .zero
            let titleSize = titleLabel?.frame.size ?? .zero
            let totalHeight = imageSize.height + titleSize.height + space
            self.imageEdgeInsets = UIEdgeInsets(top: -(totalHeight - imageSize.height), left: 0, bottom: 0, right: -titleSize.width)
            self.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageSize.width, bottom: -(totalHeight - titleSize.height) + 5.0, right: 0)
            if totalHeight > self.frame.height {
                var f = self.frame
                f.size.height = totalHeight
                self.frame = f
            }
        }
        if edge != .center {
            self.imageEdgeInsets = UIEdgeInsets(top: imageInsetsTop, left: imageInsetsLeft, bottom: imageInsetsBottom, right: imageInsetsRight)
            self.titleEdgeInsets = UIEdgeInsets(top: titleInsetsTop, left: titleInsetsLeft, bottom: titleInsetsBottom, right: titleInsetsRight)
        }
    }
}
