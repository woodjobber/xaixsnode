//
//  XYAxis+Bundle.swift
//  XYAxisSDK
//
//  Created by chengbin on 2020/12/30.
//

import Foundation

public extension Bundle {
    internal static func bundle(for aClass: AnyClass) -> Bundle? {
        let resourceBundle = Bundle(for: aClass)
        let name = "XYAxisSDK"
        let resourcePath = resourceBundle.path(forAuxiliaryExecutable: "Frameworks")
        if var resourcePath = resourcePath {
            resourcePath.append("/\(name).framework")
            let associateBundle = Bundle(path: resourcePath)
            guard let bundlePath = associateBundle?.path(forResource: name, ofType: "bundle") else { return nil }
            return Bundle(path: bundlePath)
        }
        return Bundle.bundle(for: name, aClass: aClass)
    }
    /**
     Returns a newly initialized `Bundle` instance .
     
     - Parameter name: bundle 的name 与 framework 的 name一样
     - Parameter aClass: 类
     - Returns: An initialized `Bundle` object or nil if the object couldn't be created.
     */
    static func bundle(for name: String, aClass: AnyClass) -> Bundle? {
        guard !name.isEmpty else {
            return nil
        }
//         先拿到最外面的 bundle。
//         对 framework 链接方式来说就是 framework 的 bundle 根目录，
//         对静态库链接方式来说就是 target client 的 main bundle，
//         然后再去找下面名为 "name" 的 bundle 对象。
        var bundleName = name
        if bundleName.contains(".bundle") {
            bundleName = bundleName.components(separatedBy: ".bundle").first ?? ""
        }
    
        let bundle = Bundle(for: aClass)
        let url = bundle.url(forResource: bundleName, withExtension: "bundle")
        guard let resourceUrl = url else {
            /// 静态库链接方式
            var resourcePath = bundle.path(forAuxiliaryExecutable: "Frameworks")
            resourcePath?.append("/\(name).framework")
            if let resourcePath = resourcePath {
                let associateBundle = Bundle(path: resourcePath)
                guard let bundlePath = associateBundle?.path(forResource: name, ofType: "bundle") else { return nil }
                return Bundle(path: bundlePath)
            }
            return nil
        }
        
        /// 动态库链接方式
        return Bundle(url: resourceUrl)
    }
    /**
     Returns a newly initialized `Bundle` instance .
     
     - Parameter name: bundle 名称
     - Parameter fname: framework 名称
     - Returns: An initialized `Bundle` object or nil if the object couldn't be created.
     */
    static func bundle(for name: String, framework fname: String = "") -> Bundle? {
        if name.count == 0, fname.count == 0 {
            return nil
        }
        var bundleName = name
        var frameworkName = fname
        if frameworkName.count == 0 {
            frameworkName = bundleName
        } else if bundleName.count == 0 {
            bundleName = frameworkName
        }
        if bundleName.contains(".bundle") {
            bundleName = bundleName.components(separatedBy: ".bundle").first ?? ""
        }
        
        let resourceUrl = Bundle.main.url(forResource: bundleName, withExtension: "bundle")

        if let resourceUrl = resourceUrl {
            return Bundle(url: resourceUrl)
        }
        
        let resourcePath = Bundle.main.path(forAuxiliaryExecutable: "Frameworks")
        if var resourcePath = resourcePath {
            resourcePath.append("/\(frameworkName).framework")
            let associateBundle = Bundle(path: resourcePath)
            guard let bundlePath = associateBundle?.path(forResource: bundleName, ofType: "bundle") else { return nil }
            return Bundle(path: bundlePath)
        }
        return nil
    }
}
