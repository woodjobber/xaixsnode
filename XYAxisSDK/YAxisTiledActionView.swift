//
//  YAxisTiledActionView.swift
//  XYAxisSDK
//
//  Created by chengbin on 2020/12/28.
//

import UIKit
/// 平铺视图
///
///
/// 平铺动作视图
public class YAxisTiledActionView: UIView {
    public struct TiledItem {
        /// 文本描述
        public var text: String = ""
        /// 按钮的标题
        public var actionTitle: String = ""
        
        /// 描述文本颜色
        public var textColor = UIColor(red: 0.25, green: 0.25, blue: 0.26, alpha: 1)
        
        /// 按钮标题字体
        public var actionTitleFont: UIFont? = UIFont(name: "PingFang-SC-Regular", size: 14)

        /// 按钮标题颜色
        public var actionTitleColor = UIColor(red: 0.09, green: 0.56, blue: 0.99, alpha: 1)
        
        /// 描述文本字体
        public var textFont: UIFont? = UIFont(name: "PingFang-SC-Regular", size: 14)
        
        /// 按钮的箭头
        public var actionImagedName: String = "icon_blue_right_arrow"

        /// 按钮的箭头布局位置
        public var actionImageEdge: ImageEdge = .right
        
        /// 是否隐藏动作按钮
        public var isHiddenAction: Bool = false
        
        public init() {
            text = ""
            actionTitle = ""
        }

        public init(text: String, actionTitle: String) {
            self.text = text
            self.actionTitle = actionTitle
        }
    }

    /// 数据源
    public var items = [TiledItem]() {
        didSet {
            reloadData()
        }
    }
    
    private var actionViewContainer = UIView()
    
    /// 内容的上下左右边距，默认 `UIEdgeInsets(top: 5, left: 15, bottom: 15, right: 15)`
    public var edgeInsets = UIEdgeInsets(top: 5, left: 15, bottom: 15, right: 15)

    /// 自适应内容大小 , 默认`true`
    public var isSelfSizing: Bool = true
    
    /// 子视图之间间隔
    public var lineSpace: CGFloat = 15
    
    /// 偏好最大宽度,如果使用`AutoLayout`布局，必须设置该属性`preferredMaxLayoutWidth>0`，配合`manualCalculateHeight=true`使用
    @IBInspectable public var preferredMaxLayoutWidth: CGFloat = 0
    
    /// 手动计算高度,需要配合 `preferredMaxLayoutWidth>0 `使用,`AutoLayout`有效
    @IBInspectable public var manualCalculateHeight: Bool = false
    
    override public var backgroundColor: UIColor? {
        didSet {
            actionViewContainer.backgroundColor = backgroundColor
        }
    }

    /// 点击 按钮 回调
    public var clikedSenderCompletion: ((_ index: Int) -> Void)?
    
    private var actionViewHeights = [CGFloat]()
    private var totalActionViewContainerHeight: CGFloat = 0
    private var actionViews = [YAxisActionView]()
    override public func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .white
        actionViewContainer.frame = CGRect(x: edgeInsets.left, y: edgeInsets.top, width: frame.width - edgeInsets.left - edgeInsets.right, height: frame.height - edgeInsets.top - edgeInsets.bottom)
        addSubview(actionViewContainer)
    }
    
    private func isExistConstraints() -> Bool {
        if constraints.count > 0 {
            return true
        }
        return false
    }

    private func remakeConstraint(constant: CGFloat) {
        let constraints = self.constraints
        NSLayoutConstraint.deactivate(constraints)
        constraints.forEach { constraint in
            if constraint.firstAttribute == .height {
                constraint.constant = constant
            }
        }
        NSLayoutConstraint.activate(constraints)
    }

    public func reloadData() {
        setNeedsLayout()
        layoutIfNeeded()
        setNeedsDisplay()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        let subviews = actionViewContainer.subviews
        subviews.forEach { view in
            view.removeFromSuperview()
        }
 
        var r = frame
        if preferredMaxLayoutWidth > 0 {
            r.size.width = preferredMaxLayoutWidth
        }
                
        let rec = CGRect(x: edgeInsets.left, y: edgeInsets.top, width: r.width - edgeInsets.left - edgeInsets.right, height: r.height - edgeInsets.top - edgeInsets.bottom)
        if !rec.equalTo(actionViewContainer.frame) {
            actionViewContainer.frame = rec
        }
        if r.size.equalTo(.zero) {
            invalidateIntrinsicContentSize()
            return
        }

        while items.count < 10, actionViews.count > 10 {
            actionViews.removeLast()
        }
        for actionView in actionViews {
            actionView.isHidden = true
        }
    
        for (index, item) in items.enumerated() {
            if index >= actionViews.count {
                let actionView = createActionViewWithFrame(CGRect(x: 0, y: 0, width: actionViewContainer.frame.width, height: actionViewContainer.frame.height), item: item)
                actionView.tag = index
                actionViews.append(actionView)
            } else {
                let actionView = actionViews[index]
                actionView.tag = index
                actionView.isHidden = false
                decoratorActionView(actionView, item: item)
            }
        }
        /// 计算高度之前
        var f = actionViewContainer.frame
        let totalHeight = calculateActionViewContinaerHeight()
        if totalHeight >= 0 {
            f.size.height = totalHeight
        }
        /// 计算高度之后
        if !f.equalTo(actionViewContainer.frame) {
            actionViewContainer.frame = CGRect(x: edgeInsets.left, y: edgeInsets.top, width: frame.width - edgeInsets.left - edgeInsets.right, height: f.height)
        }
        
        let count = items.count
        
        if count == 1 {
            let actionView = actionViews.first!
            actionView.frame = CGRect(x: 0, y: 0, width: f.width, height: actionViewHeights.first ?? 0)
            actionView.tag = 0
            actionView.clikedSenderCallback = {
                [weak self] idx in
                self?.clikedSenderCompletion?(idx)
            }
            actionViewContainer.addSubview(actionView)
        } else {
            /// 记录前一个 `YAxisActionView`的最大 `Y`坐标
            var preMaxY: CGFloat = 0
            /// 当前 `YAxisActionView` 的 `y`值
            var y: CGFloat = 0
            for (index, _) in items.enumerated() {
                let height = actionViewHeights[index]
                if index > 0 {
                    y = preMaxY + lineSpace
                }
                let actionView = actionViews[index]
                actionView.frame = CGRect(x: 0, y: y, width: f.width, height: height)
                actionView.tag = index
                actionView.clikedSenderCallback = {
                    [weak self] idx in
                    self?.clikedSenderCompletion?(idx)
                }
                actionViewContainer.addSubview(actionView)
                
                preMaxY = actionView.frame.maxY
            }
        }
    
        let height = totalHeight <= 0 ? 0 : totalHeight + edgeInsets.top + edgeInsets.bottom
       
        var frame = self.frame
        frame.size.height = height
        if !existConatriantHeight() {
            if !self.frame.equalTo(frame), isSelfSizing {
                self.frame = frame
            } else if frame.height < height, !self.frame.equalTo(frame) {
                self.frame = frame
            }
        }
        var h = frame.height - edgeInsets.top - edgeInsets.bottom
        if existConatriantHeight(), !self.frame.equalTo(frame), isSelfSizing {
            /// 重新设定约束高度
            remakeConstraint(constant: height)
        }
        if h < 0 {
            h = 0
        }
        let rect = CGRect(x: edgeInsets.left, y: edgeInsets.top, width: frame.width - edgeInsets.left - edgeInsets.right, height: h)
        if !actionViewContainer.frame.equalTo(rect) {
            actionViewContainer.frame = rect
        }
        if preferredMaxLayoutWidth > 0, manualCalculateHeight {
            invalidateIntrinsicContentSize()
        }
    }
    
    /// 判断是否存在 高度约束
    private func existConatriantHeight() -> Bool {
        guard isExistConstraints() else {
            return false
        }
        let constraints = self.constraints
        var exist = false
        for constraint in constraints {
            if constraint.firstAttribute == .height, constraint.secondAttribute == .notAnAttribute {
                exist = true
                break
            }
        }
        return exist
    }

    /// 创建一个`YAxisActionView` 对象
    private func createActionViewWithFrame(_ frame: CGRect, item: TiledItem) -> YAxisActionView {
        let actionView = YAxisActionView(frame: frame)
        actionView.actionTitleFont = item.actionTitleFont
        actionView.actionTitleColor = item.actionTitleColor
        actionView.textColor = item.textColor
        actionView.actionTitle = item.actionTitle
        actionView.text = item.text
        actionView.textFont = item.textFont
        actionView.imageEdge = item.actionImageEdge
        actionView.actionImagedName = item.actionImagedName
        actionView.isHiddenAction = item.isHiddenAction
        return actionView
    }

    private func decoratorActionView(_ actionView: YAxisActionView, item: TiledItem) {
        actionView.actionTitleFont = item.actionTitleFont
        actionView.actionTitleColor = item.actionTitleColor
        actionView.textColor = item.textColor
        actionView.actionTitle = item.actionTitle
        actionView.text = item.text
        actionView.textFont = item.textFont
        actionView.imageEdge = item.actionImageEdge
        actionView.actionImagedName = item.actionImagedName
        actionView.isHiddenAction = item.isHiddenAction
    }

    /// 计算 `YAxisActionView`的总高度
    private func calculateActionViewContinaerHeight() -> CGFloat {
        actionViewHeights.removeAll()
        var total: CGFloat = 0
        if items.count == 0 {
            return 0
        }
        for (index, _) in items.enumerated() {
            let actionView = actionViews[index]
            actionView.__layoutSubviews()
            let height = actionView.frame.height
            actionViewHeights.append(height)
            if index == items.count - 1 {
                total += height
            } else {
                total += (height + lineSpace)
            }
        }
        totalActionViewContainerHeight = total
        return total
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    /// 实际内容大小，不包含`edgeInsets`
    public var contentSize: CGSize {
        setNeedsLayout()
        layoutIfNeeded()
        let size = intrinsicContentSize
        guard totalActionViewContainerHeight > 0 else {
            return CGSize(width: frame.width - edgeInsets.left - edgeInsets.right, height: 0)
        }
        return CGSize(width: size.width - edgeInsets.left - edgeInsets.right, height: size.height - edgeInsets.top - edgeInsets.bottom)
    }
    
    /// 固有内容大小，包含`edgeInsets`
    override public var intrinsicContentSize: CGSize {
        guard totalActionViewContainerHeight > 0 else {
            return CGSize(width: frame.width, height: 0)
        }
        let height = totalActionViewContainerHeight + edgeInsets.bottom + edgeInsets.top
        var f = frame
        f.size.height = height
        if preferredMaxLayoutWidth > 0 {
            f.size.width = preferredMaxLayoutWidth
        }
        return CGSize(width: f.width, height: f.height)
    }
    
    override public func prepareForInterfaceBuilder() {
        invalidateIntrinsicContentSize()
    }
}

private class YAxisActionView: UIView {
    lazy var descLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "PingFang-SC-Regular", size: 14)
        label.textColor = UIColor(red: 0.25, green: 0.25, blue: 0.26, alpha: 1)
        label.numberOfLines = 0
        addSubview(label)
        return label
    }()

    lazy var descButton: UIButton = {
        let button = UIButton(type: .custom)
        button.isUserInteractionEnabled = true
        button.setImage(UIImage.image(named: "icon_blue_right_arrow"), for: .normal)
        button.titleLabel?.font = UIFont(name: "PingFang-SC-Regular", size: 14)
        addSubview(button)
        button.addTarget(self, action: #selector(clikcedButton(_:)), for: .touchUpInside)
        return button
    }()

    var text: String? = "" {
        didSet {
            descLabel.text = text
        }
    }

    var actionTitle: String? = "" {
        didSet {
            descButton.setTitle(actionTitle, for: .normal)
        }
    }
    
    var textFont: UIFont? = UIFont(name: "PingFang-SC-Regular", size: 14) {
        didSet {
            descLabel.font = textFont
        }
    }
    
    var textColor = UIColor(red: 0.25, green: 0.25, blue: 0.26, alpha: 1) {
        didSet {
            descLabel.textColor = textColor
        }
    }
    
    var actionTitleFont: UIFont? = UIFont(name: "PingFang-SC-Regular", size: 14) {
        didSet {
            descButton.titleLabel?.font = actionTitleFont
        }
    }
    
    var actionTitleColor = UIColor(red: 0.09, green: 0.56, blue: 0.99, alpha: 1) {
        didSet {
            descButton.setTitleColor(actionTitleColor, for: .normal)
        }
    }
    
    var clikedSenderCallback: ((_ index: Int) -> Void)?
    
    var actionImagedName: String = "icon_blue_right_arrow"
    
    var imageEdge: ImageEdge = .right
    
    var isHiddenAction: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override var tag: Int {
        didSet {
            descButton.tag = tag
        }
    }
    
    @objc func clikcedButton(_ sender: UIButton) {
        clikedSenderCallback?(sender.tag)
    }

    override var intrinsicContentSize: CGSize {
        let height = descButton.frame.maxY
        return CGSize(width: frame.width, height: height)
    }

    override func prepareForInterfaceBuilder() {
        invalidateIntrinsicContentSize()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if frame.equalTo(.zero) {
            return
        }
        __layoutSubviews()
        invalidateIntrinsicContentSize()
    }

    func __layoutSubviews() {
        if actionImagedName.isEmpty {
            descButton.setImage(nil, for: .normal)
        } else {
            descButton.setImage(UIImage.image(named: actionImagedName), for: .normal)
        }
        if isHiddenAction {
            descButton.isHidden = true
            var f = descButton.frame
            f.size.height = 0
            descButton.frame = f
        } else {
            descButton.isHidden = false
            descButton.sizeToFit()
            descButton.imageEdge(imageEdge)
        }

        descLabel.text = text
    
        let textString = NSString(string: text ?? "")
        let size = CGSize(width: frame.width, height: .greatestFiniteMagnitude)
        let rect = textString.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: textFont as Any], context: nil)
        if textString.length == 0 {
            descLabel.frame = CGRect(x: 0, y: 0, width: size.width, height: 0)
        } else {
            descLabel.frame = CGRect(x: 0, y: 0, width: size.width, height: rect.height)
        }
    
        var buttonFrame = descButton.frame
        
        let y = (isHiddenAction || (actionTitle?.isEmpty ?? true) && actionImagedName.isEmpty) ? descLabel.frame.maxY : descLabel.frame.maxY + 10
    
        buttonFrame = CGRect(x: 0, y: y, width: buttonFrame.width, height: buttonFrame.height)

        descButton.frame = buttonFrame
        
        let frame = self.frame
        let buttonSize = CGSize(width: frame.width, height: buttonFrame.minY + buttonFrame.height)
        
        if !frame.size.equalTo(buttonSize) {
            self.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: buttonFrame.maxY)
        }
    }
}

private extension UIImage {
    /// 获取图片资源
    /// - Parameter name: 图片名
    static func image(named name: String) -> UIImage? {
        let bundle = Bundle.bundle(for: YAxisTiledActionView.self)
        let image = UIImage(named: name, in: bundle, compatibleWith: nil)
        if image == nil {
            return UIImage(named: name)
        }
        return image
    }
}
